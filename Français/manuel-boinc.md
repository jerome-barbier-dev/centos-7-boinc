---
title: "Petit manuel pour la mise en place de BOINC sur Centos 7"
author: Jérôme Barbier
date: avril 2020
geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"
output: pdf_document
---


# Présentation de BOINC
BOINC est l'acronyme de *Berkeley Open Infrastructure for Network Computing*, ou en français, *Infrastructure Ouverte pour le Calcul en Réseau de Berkeley*.

Il s'agit d'un ensemble de logiciels pour serveurs et ordinateurs personnels permettant de mutualiser des ressources de calcul pour pouvoir calculer des problèmes complexes, notamment universitaires.

Un tel système présente deux avantages :

  * Il a un coût de fonctionnement faible pour l'université
  * Il permet d'atteindre une puissance de calcul élevée

Afin de comparer des puissances de calcul, on compte en FLOP par seconde (noté FLOPS ou FLOP/s), qui représente le nombre de calculs en virgule flottante (un calcul sur un nombre à virgule) que peut faire un ordinateur ou un groupe d'ordinateurs.  
Pour calculer le nombre de FLOPS d'un processeur, on peut utiliser la formule suivante :

FLOPS = Nombre de coeurs du processeur × Fréquence du processeur × (FLOP / Cycle)

La partie *FLOP / Cycle* représente le nombre de calculs en virgule flottante que le processeur fait pour un cycle de calcul.  
Etant dépendante de la fréquence du processeur et du nombre de coeurs du processeur, cette valeur a beaucoup augmenté ces dernières années.  
Par exemple, le *Intel 486DX4-75 PGA*, sorti en 1994, monocoeur et cadencé à 75 Mhz fournissait 0,00308 GFLOPS, 14 ans plus tard en 2008, le *Intel Core 2 Duo T9500*, avec ses 2 coeurs à 2,60 Ghz atteignait 20,4 GFLOPS. Enfin, en 2017, le *Intel i7-7700*, avec 4 coeurs de 2 threads chacun oscillant entre 3,6 et 4,2 GHz atteignait 255,26 GFLOPS.

Pour en revenir au calcul distribué, l'université de Berkeley chiffre ainsi l'usage de BOINC pour une puissance de calcul de 100 TeraFLOPS sur une année :

  * Chez Amazon, avec son système Elastic Computing Clound : 175 millions de dollars
  * En construisant un cluster (ensemble d'ordinateurs) : 12, 4 millions de dollars (en incluant tous les coûts annexes comme la climatisation, le matériel réseau, l'électricité, les administrateurs système...)
  * En utilisant BOINC : 125 000 dollars

On voit donc l'intérêt de BOINC puisqu'il est près de 100 fois moins cher que la moins chère des solutions "standard" (99,2 fois pour être précis).

De plus, l'intérêt de mutualiser ses ressources permet d'effectuer des calculs dans un laps de temps bien moindre que sans mutualisation. En effet, chaque ordinateur sur le réseau peut se charger d'une petite partie d'un calcul complexe, et le faire en même temps que les autres ordinateurs. Une fois tous les calculs réalisés, un superviseur se charge de collecter les résultats partiels et de les agréger pour en déduire le résultat final du calcul.

Par exemple, si l'on considère un ensemble d'ordinateurs personnels d'une puissance de calcul de 10 GigaFLOPS (ce qui est la puissance d'un processeur monocoeur de 2,5 Ghz de l'année 2011, donc ni récent, ni véloce par rapport à ce qu'on fait aujourd'hui), et en gardant l'hypothèse qu'il y ait besoin de 100 TeraFLOPS pendant une année, alors le temps de calcul évolue ainsi :

  * Avec 1 seul ordinateur, il faudrait 10 000 ans pour compléter le calcul
  * Avec 2 ordinateurs, il faudrait 5 000 ans pour compléter le calcul (plus un léger délais pour agréger les résultats)
  * Avec 10 000 ordinateurs, il faudrait une année pour compléter le calcul (là encore avec un délais supplémentaire), on atteint en fait la puissance de calcul nécessaire
  * Avec 20 000 ordinateurs, il faudrait 6 mois pour compléter le calcul (là encore avec un délais supplémentaire), on double la capacité de calcul nécessaire

En 2019, BOINC fonctionnait avec 477 414 ordinateurs, pour une puissance de calcul totale de 33,82 PetaFLOPS (33 820 TeraFLOPS)

Avec ces avantages, BOINC est utilisé pour de nombreux projets scientifiques :

  * Rosetta@home, qui fait de la recherche sur les structures protéiques
  * SETI@home, qui est lancé directement par l'université de Berkeley, est à la recherche d'intelligence extraterrestre
  * LHC@home, qui est lancé par le CERN, fait de la recherche en physique

Vous pouvez trouver davantage de projets à cette adresse : [liste des projets sur le site de l'université de Berkeley](https://boinc.berkeley.edu/projects.php)  
Si vous envisagez d'utiliser Centos pour calculer, assurez-vous que le logo *Linux* soit listé dans la colonne des systèmes compatibles et si vous utilisez Centos sur ARM (comme sur un Raspberry Pi), assurez-vous que le logo *Linux ARM* soit affiché.

# Pré-requis
Pour installer BOINC sur Centos 7, en plus d'un système Centos installé et connecté à Internet, il va vous falloir activer le dépôt *epel* :

`sudo yum install epel-release`

Créez vous ensuite un compte sur le site du projet de votre choix.  
Pour ce tutoriel, j'utiliserai le projet [http://boinc.bakerlab.org/rosetta/](Rosetta), auquel j'ai dédié un ordinateur chez moi.

# Installation des paquets
Un seul packet est requis pour utiliser BOINC : *boinc-client*. Vous pouvez également installer *boinc-manager* pour avoir un interface graphique permettant de visualiser ce qu'il se passe et gérer les projets (mais vous pouvez aussi le faire en ligne de commande avec *boinc-client*).

`sudo yum install boinc-client boinc-manager`

# S'assurer que l'installation fonctionne
Une fois l'installation terminée, essayez de taper la commande suivante :

`boinccmd --get_tasks`

Si le message que vous obtenez indique une erreur, retenez qu'il vous faudra vous trouver dans le dossier /var/lib/boinc pour pouvoir utiliser la commande.

Si vous avez installé *boinc-manager*, essayez de le lancer. Si l'interface vous affiche "Déconnecté" en bas à droite de la fenêtre, alors vous avez un petit souci de permissions sur les fichiers de BOINC.

Pour régler ce souci, changez les permissions du fichier *gui_rpc_auth.cfg* :

`sudo chmod o+r /var/lib/boinc/gui_rpc_auth.cfg`

Changez également le dossier dans lequel le manager va aller lire la configuration du client BOINC.  
Pour cela, éditez le fichier */usr/share/applications/boinc-manager.desktop* (avec *vim*, *gedit* ou tout autre éditeur) :

`sudo vim /usr/share/applications/boinc-manager.desktop`

Et modifiez la ligne suivante :

`Exec=boincmgr`

En :

`Exec=boincmgr -e /var/lib/boinc`

Puis sauvegardez, votre manager devrait maintenant fonctionner.

# Rejoindre un projet

Pour rejoindre un projet, vous avez deux solutions :

  * Soit utiliser le manager, menu *Outils* puis *Ajouter un projet*, un assistant vous guidera
  * Soit utiliser quelques commandes (2 pour être précis), c'est la solution que je préfère !

Pour rejoindre un projet via ligne de commande, connectez votre client BOINC au compte que vous avez sur le projet :

`boinccmd --lookup_account http://boinc.bakerlab.org/rosetta/ <Votre adresse email> <Votre mot de passe>`

Vous devriez voir les lignes suivantes défiler sur votre écran :

```
status: Success
poll status: operation in progress
poll status: operation in progress
account key: ××××××
```

Retenez votre *Account key*. Ensuite, il faut dire à BOINC qu'il doit travailler pour ce projet :

`boinccmd --project_attach  http://boinc.bakerlab.org/rosetta/ <Votre Account key>`

Une fois ceci fait, votre ordinateur se mettra à calculer pour le projet de votre choix ! Félicitations !

Vous pouvez suivre l'avancement des tâches soit via l'interface de BOINC manager, soit via la commande suivante :

`boinccmd --get_tasks`

Pour avoir accès à plus de vues et de fonctionnalités sur BOINC manager, vous pouvez le passer en mode *avancé* en allant dans les menus *Affichage* puis *Vue avancée...*.

![Vue de BOINC Manager](boinc-manager.png)

# Références
  * Chiffrages de l'université de Berkeley : [Université de Berkeley](https://boinc.berkeley.edu/trac/wiki/BoincOverview)
  * Puissances de calcul des processeurs donnés en exemple : [Répertoire de puissances de calcul](https://github.com/Mysticial/Flops)
  * Explications de Wikipedia sur le FLOPS : [Wikipédia - Le FLOPS](https://fr.wikipedia.org/wiki/FLOPS)
